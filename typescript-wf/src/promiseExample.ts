
    export class PromiseExample
    {
        private _timeoutMs : number = 3000;
        private _asyncMs : number = 2000;

        constructor() 
        {
            
        }

        public async init()
        {
            console.log("Calling the async function");

            try
            {
                var res = await this.testAsync();
            }
            catch ( e )
            {
                var res = e as string;
            }

            console.log(res);
        }

        public async testAsync()
        {
            var res = await new Promise( (resolve, reject) => {
                var timoutId = setTimeout(() => {
                    reject('Gave up waiting. Fail');
                }, this._timeoutMs);

                setTimeout(() => {
                    clearTimeout(timoutId);
                    resolve('🤡やった！出来た');
                }, this._asyncMs);
              });

            return res + '<--- this guy!';
        }
    }
