export class SomeClass
{
	constructor()
	{
		console.log(`SomeClass has been instantiated`);
	}

	public publicMethod() : void
	{
		console.log(`public method call`);
		this.privateMethod();
	}

	private privateMethod() : void
	{
		console.log(`private method call`);
	}
}