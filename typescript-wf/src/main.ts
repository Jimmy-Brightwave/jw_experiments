import '../node_modules/promise-polyfill/src/polyfill';
import '../node_modules/jquery/src/jquery';

import { SomeClass } from "./bw/someClass";
import { PromiseExample } from "./promiseExample";
import { Helper as Debug } from './bw/helper';

declare function require(...args) : any;

//-- Would not work!
// import { StateMachine } from '../node_modules/javascript-state-machine/dist/state-machine';

//-- Apprently an anti-pattern
var StateMachine = require('../node_modules/javascript-state-machine/dist/state-machine');

// type StateMachine

export class Main
{

	private _classInstance : SomeClass;
	private _fsm : any;

	constructor()
	{
		
		this._classInstance = new SomeClass();
		this._classInstance.publicMethod();

		this._fsm = new StateMachine({
			init: 'solid',
			transitions: [
			  { name: 'melt',     from: 'solid',  to: 'liquid' },
			  { name: 'freeze',   from: 'liquid', to: 'solid'  },
			  { name: 'vaporize', from: 'liquid', to: 'gas'    },
			  { name: 'condense', from: 'gas',    to: 'liquid' }
			],
			methods: {
			  onMelt:     function() { console.log('I melted')    },
			  onFreeze:   function() { console.log('I froze')     },
			  onVaporize: function() { console.log('I vaporized') },
			  onCondense: function() { console.log('I condensed') }
			}
		  });

		this._fsm.melt();

		this.testJquery();
		this.testArrowFunction();
		this.testAsyncFunction();
	}

	private testJquery()
	{
		var elm = $('<p>HELLO THIS IS AN ELEMENT ADDED BY JQUERY!</p>');
		$('body').append(elm);		
	}

	private testArrowFunction() 
	{
		Debug.log("Testing arrow functions");
		var someArray = [1,2,3,4,5,6];
		someArray.forEach( elm => {
			Debug.log(elm);
		});
	}

	async testAsyncFunction()
	{
		Debug.log(`Testing async function wait for it...`);

		var promiseExample = new PromiseExample();
		
		for (var i = 0; i < 3; i++)
		{
			let asyncElm = $(`<p>some stuff ${ await promiseExample.testAsync() }</p>`);
			$('body').append(asyncElm);

		}

		Debug.log(`All done!`);
	}

}

var wf = new Main();