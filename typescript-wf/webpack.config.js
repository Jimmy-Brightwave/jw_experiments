const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
  watch: true,	
  entry: ['./src/main.ts'],
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
  extensions: [ '.tsx', '.ts', '.js' ]
  },  
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  optimization: {
  minimize : true,
    minimizer: [
      new TerserPlugin({
        cache: true,
        parallel: true,
        sourceMap: true, // Must be set to true if using source-maps in production
        terserOptions: {
        compress: false,
        mangle: true // Note `mangle.properties` is `false` by default.
        }
      }),
    ],
  }
};