"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var Main = /** @class */ (function () {
    function Main() {
        var _this = this;
        this.canvas = document.getElementById("canvas");
        this.gl = this.canvas.getContext("webgl") || this.canvas.getContext("experimental-webgl");
        this.originalImage = { width: 1, height: 1 }; // replaced after loading
        this.originalTexture = twgl.createTexture(this.gl, {
            src: "img/original.jpg",
            crossOrigin: '',
        }, function (err, texture, source) {
            _this.originalImage = source;
        });
        this.mapTexture = twgl.createTexture(this.gl, {
            src: "img/depthmap.jpg", crossOrigin: '',
        });
        // calls gl.createBuffer, gl.bindBuffer, gl.bufferData for a quad
        this.bufferInfo = twgl.primitives.createXYQuadBufferInfo(this.gl);
        this.mouse = [0, 0];
        document.addEventListener('mousemove', function (event) {
            _this.mouse[0] = (event.clientX / _this.gl.canvas.clientWidth * 2 - 1) * -0.02;
            _this.mouse[1] = (event.clientY / _this.gl.canvas.clientHeight * 2 - 1) * -0.02;
        });
        document.addEventListener('touchmove', function (event) {
            _this.mouse[0] = (event.touches[0].clientX / _this.gl.canvas.clientWidth * 2 - 1) * -0.02;
            _this.mouse[1] = (event.touches[0].clientY / _this.gl.canvas.clientHeight * 2 - 1) * -0.02;
        });
        document.addEventListener('touchend', function (event) {
            _this.mouse[0] = 0;
            _this.mouse[1] = 0;
        });
        this.nMouse = [0, 0];
        this.oMouse = [0, 0];
        this.render = this.render.bind(this);
        this.loadShaders();
    }
    Main.prototype.getRequest = function (url) {
        return new Promise(function (resolve, reject) {
            var request = new XMLHttpRequest();
            request.onload = function () {
                if (this.status === 200) {
                    resolve(this.response);
                }
                else {
                    reject(new Error(this.statusText));
                }
            };
            request.onerror = function () {
                reject(new Error('XMLHttpRequest Error: ' + this.statusText));
            };
            request.open('GET', url);
            request.send();
        });
    };
    Main.prototype.loadShaders = function () {
        return __awaiter(this, void 0, void 0, function () {
            var vs, fs;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getRequest("shader/vertex.vert")];
                    case 1:
                        vs = _a.sent();
                        return [4 /*yield*/, this.getRequest("shader/fragment.frag")];
                    case 2:
                        fs = _a.sent();
                        // compile shaders, link program, lookup location
                        this.programInfo = twgl.createProgramInfo(this.gl, [vs, fs]);
                        requestAnimationFrame(this.render);
                        return [2 /*return*/];
                }
            });
        });
    };
    Main.prototype.render = function () {
        twgl.resizeCanvasToDisplaySize(this.gl.canvas);
        this.gl.viewport(0, 0, this.gl.canvas.width, this.gl.canvas.height);
        this.gl.clearColor(0, 0, 0, 0);
        this.gl.clear(this.gl.COLOR_BUFFER_BIT);
        this.gl.useProgram(this.programInfo.program);
        // calls gl.bindBuffer, gl.enableVertexAttribArray, gl.vertexAttribPointer
        twgl.setBuffersAndAttributes(this.gl, this.programInfo, this.bufferInfo);
        var canvasAspect = this.gl.canvas.clientWidth / this.gl.canvas.clientHeight;
        var imageAspect = this.originalImage.width / this.originalImage.height;
        var mat = m3.scaling(imageAspect / canvasAspect, -1);
        this.nMouse[0] += (this.mouse[0] - this.nMouse[0]) * 0.05; // * 35;
        this.nMouse[1] += (this.mouse[1] - this.nMouse[1]) * 0.05; // * 35;
        // calls gl.activeTexture, gl.bindTexture, gl.uniformXXX
        twgl.setUniforms(this.programInfo, {
            u_matrix: mat,
            u_originalImage: this.originalTexture,
            u_mapImage: this.mapTexture,
            u_mouse: this.nMouse,
        });
        // calls gl.drawArrays or gl.drawElements
        twgl.drawBufferInfo(this.gl, this.bufferInfo);
        requestAnimationFrame(this.render);
    };
    return Main;
}());
var main = new Main();
//# sourceMappingURL=Main.js.map