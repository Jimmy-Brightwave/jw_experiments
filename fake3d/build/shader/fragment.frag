	precision mediump float;

	uniform vec2 u_mouse;

	uniform sampler2D u_originalImage;
	uniform sampler2D u_mapImage;

	varying vec2 v_texcoord;

	void main() {
		 vec4 depthDistortion = texture2D(u_mapImage, v_texcoord);
		 float parallaxMult = depthDistortion.r;

		 vec2 parallax = (u_mouse) * parallaxMult;

		 vec4 original = texture2D(u_originalImage, (v_texcoord + parallax));
		 gl_FragColor = original;
	}