class Main
{
	private canvas:HTMLCanvasElement = document.getElementById("canvas") as HTMLCanvasElement;
	private gl:WebGLRenderingContext = this.canvas.getContext("webgl") || this.canvas.getContext("experimental-webgl") as WebGLRenderingContext;
	private originalImage : any;

	private originalTexture : any;
	private mapTexture : any;

	private programInfo : any;
	private bufferInfo : any;
	
	private mouse : Array<number>;
	private nMouse : Array<number>;
	private oMouse : Array<number>;

	constructor()
	{
		this.originalImage = { width: 1, height: 1 }; // replaced after loading

		this.originalTexture = twgl.createTexture(this.gl, {
		  src: "img/original.jpg", 
		  crossOrigin: '',
		}, (err : any, texture : any, source : any) => {
		  this.originalImage = source;
		});
		
		this.mapTexture = twgl.createTexture(this.gl, {
		  src: "img/depthmap.jpg", crossOrigin: '',
		});
			  
		// calls gl.createBuffer, gl.bindBuffer, gl.bufferData for a quad
		this.bufferInfo = twgl.primitives.createXYQuadBufferInfo(this.gl);
	  
		this.mouse = [0, 0];
		document.addEventListener('mousemove', (event) => {
		  this.mouse[0] = (event.clientX / this.gl.canvas.clientWidth  * 2 - 1) * -0.02;
		  this.mouse[1] = (event.clientY / this.gl.canvas.clientHeight * 2 - 1) * -0.02;
		});
		  
		  document.addEventListener('touchmove', (event) => {
			this.mouse[0] = (event.touches[0].clientX / this.gl.canvas.clientWidth  * 2 - 1) * -0.02;
			this.mouse[1] = (event.touches[0].clientY / this.gl.canvas.clientHeight * 2 - 1) * -0.02;
		});
		  
		  document.addEventListener('touchend', (event) => {
			this.mouse[0] = 0;
			this.mouse[1] = 0;
		});
		  
		this.nMouse = [0, 0];
		this.oMouse = [0, 0];
		
		this.render = this.render.bind(this);
		
		this.loadShaders();
	}


	getRequest(url: string): Promise<any> {
		return new Promise<any>(
		  function (resolve, reject) {
			const request = new XMLHttpRequest();
			request.onload = function () {
			  if (this.status === 200) {
				resolve(this.response);
			  } else {
				reject(new Error(this.statusText));
			  }
			};
			request.onerror = function () {
			  reject(new Error('XMLHttpRequest Error: ' + this.statusText));
			};
			request.open('GET', url);
			request.send();
		  });
	}
	

	async loadShaders()
	{
		var vs = await this.getRequest("shader/vertex.vert");
		var fs = await this.getRequest("shader/fragment.frag");

		// compile shaders, link program, lookup location
		
		this.programInfo = twgl.createProgramInfo(this.gl, [vs, fs]);

		requestAnimationFrame(this.render);
	}

	private render()
	{
		twgl.resizeCanvasToDisplaySize(this.gl.canvas);

		this.gl.viewport(0, 0, this.gl.canvas.width, this.gl.canvas.height);
	
		this.gl.clearColor(0, 0, 0, 0);
		this.gl.clear(this.gl.COLOR_BUFFER_BIT);
	
		this.gl.useProgram(this.programInfo.program);
	
		// calls gl.bindBuffer, gl.enableVertexAttribArray, gl.vertexAttribPointer
		twgl.setBuffersAndAttributes(this.gl, this.programInfo, this.bufferInfo);
	
		const canvasAspect = this.gl.canvas.clientWidth / this.gl.canvas.clientHeight;
		const imageAspect = this.originalImage.width / this.originalImage.height;
		const mat = m3.scaling(imageAspect / canvasAspect, -1);
			
			this.nMouse[0] += (this.mouse[0] - this.nMouse[0]) * 0.05;// * 35;
			this.nMouse[1] += (this.mouse[1] - this.nMouse[1]) * 0.05;// * 35;
				
		// calls gl.activeTexture, gl.bindTexture, gl.uniformXXX
		twgl.setUniforms(this.programInfo, {
		  u_matrix: mat,
		  u_originalImage: this.originalTexture,
		  u_mapImage: this.mapTexture,
		  u_mouse: this.nMouse,
		});
			
		// calls gl.drawArrays or gl.drawElements
		twgl.drawBufferInfo(this.gl, this.bufferInfo);
		
		requestAnimationFrame(this.render);
	}
}

var main = new Main();