(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"complain_atlas_", frames: [[1491,1401,322,389],[0,1565,340,397],[1471,1044,374,355],[342,1565,372,357],[716,1401,369,352],[1087,1401,402,317],[1463,0,373,410],[1087,1720,398,317],[1471,737,527,305],[0,1205,630,358],[760,834,709,390],[0,433,758,406],[1457,433,584,302],[0,841,628,362],[760,433,695,399],[0,0,738,428],[740,0,721,431]]},
		{name:"complain_atlas_2", frames: [[1365,1585,226,219],[1580,1035,276,189],[359,1057,232,316],[1775,0,234,343],[701,1332,212,307],[838,1019,433,135],[1150,1156,428,126],[1154,1284,179,297],[963,1625,179,302],[1335,1284,177,299],[1189,303,242,291],[246,1375,228,308],[476,1375,223,304],[593,1030,243,300],[589,719,242,309],[476,1681,232,291],[0,315,364,335],[0,992,357,334],[0,652,357,338],[0,0,399,313],[401,0,372,304],[1146,0,369,301],[775,0,369,302],[401,306,469,204],[1379,602,221,262],[1724,1456,205,247],[1514,1284,208,253],[833,742,221,275],[359,719,228,336],[1517,0,256,386],[366,512,449,205],[0,1328,244,409],[1724,1226,226,228],[1344,866,220,229],[1144,1625,219,228],[872,304,315,229],[0,1739,474,208],[1433,388,304,212],[1056,838,286,177],[1422,1806,277,176],[1144,1855,276,177],[1602,602,292,193],[833,535,300,205],[710,1641,251,251],[1135,596,242,240],[1602,797,235,236],[915,1156,233,236],[1775,345,227,220],[1701,1705,225,216],[915,1394,237,229]]},
		{name:"complain_atlas_3", frames: [[228,0,262,185],[492,0,256,188],[228,190,404,117],[881,204,115,323],[881,529,115,323],[210,719,114,324],[0,827,123,289],[326,805,119,275],[758,804,116,288],[876,854,122,269],[636,804,120,281],[447,805,120,253],[750,0,225,202],[441,416,220,195],[212,522,218,195],[663,416,216,194],[0,217,226,205],[663,612,211,190],[0,634,208,191],[432,613,202,190],[634,204,214,210],[228,309,211,211],[0,0,226,215],[0,424,210,208]]}
];


// symbols:



(lib.CachedTexturedBitmap_1 = function() {
	this.initialize(img.CachedTexturedBitmap_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,3881,2209);


(lib.CachedTexturedBitmap_10 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_11 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_12 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_13 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_14 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_15 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_16 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_17 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_18 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_19 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_2 = function() {
	this.initialize(ss["complain_atlas_"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_20 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_21 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_22 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_23 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_24 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_25 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_26 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_27 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_28 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_29 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_3 = function() {
	this.initialize(ss["complain_atlas_"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_30 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_31 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_32 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_33 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_34 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_35 = function() {
	this.initialize(ss["complain_atlas_"]);
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_36 = function() {
	this.initialize(ss["complain_atlas_"]);
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_37 = function() {
	this.initialize(ss["complain_atlas_"]);
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_38 = function() {
	this.initialize(ss["complain_atlas_"]);
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_39 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_4 = function() {
	this.initialize(ss["complain_atlas_"]);
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_40 = function() {
	this.initialize(ss["complain_atlas_"]);
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_41 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_42 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_43 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_44 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_45 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_46 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_47 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_48 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_49 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_5 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_50 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_51 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(25);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_52 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(26);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_53 = function() {
	this.initialize(ss["complain_atlas_"]);
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_54 = function() {
	this.initialize(ss["complain_atlas_"]);
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_55 = function() {
	this.initialize(ss["complain_atlas_"]);
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_56 = function() {
	this.initialize(ss["complain_atlas_"]);
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_57 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(27);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_58 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(28);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_59 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(29);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_6 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(30);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_60 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(31);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_61 = function() {
	this.initialize(ss["complain_atlas_"]);
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_62 = function() {
	this.initialize(ss["complain_atlas_"]);
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_63 = function() {
	this.initialize(ss["complain_atlas_"]);
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_64 = function() {
	this.initialize(ss["complain_atlas_"]);
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_65 = function() {
	this.initialize(ss["complain_atlas_"]);
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_66 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(32);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_67 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(33);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_68 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(34);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_69 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(35);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_7 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(36);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_70 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(37);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_71 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(38);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_72 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(39);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_73 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(40);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_74 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(41);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_75 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(42);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_76 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_77 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_78 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_79 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_8 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_80 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_81 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_82 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_83 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(43);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_84 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(44);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_85 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(45);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_86 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(46);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_87 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(47);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_88 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_89 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_9 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_90 = function() {
	this.initialize(ss["complain_atlas_3"]);
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_91 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(48);
}).prototype = p = new cjs.Sprite();



(lib.CachedTexturedBitmap_92 = function() {
	this.initialize(ss["complain_atlas_2"]);
	this.gotoAndStop(49);
}).prototype = p = new cjs.Sprite();



(lib.Scene_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.CachedTexturedBitmap_1();
	this.instance.parent = this;
	this.instance.setTransform(-3.95,-11.95,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(195));

}).prototype = p = new cjs.MovieClip();


(lib.mouth1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_83();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_84();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,1.05,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_85();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0.45,0.6,0.3846,0.3846);

	this.instance_3 = new lib.CachedTexturedBitmap_86();
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.45,-0.1,0.3846,0.3846);

	this.instance_4 = new lib.CachedTexturedBitmap_87();
	this.instance_4.parent = this;
	this.instance_4.setTransform(0,0.65,0.3846,0.3846);

	this.instance_5 = new lib.CachedTexturedBitmap_88();
	this.instance_5.parent = this;
	this.instance_5.setTransform(0,1.1,0.3846,0.3846);

	this.instance_6 = new lib.CachedTexturedBitmap_89();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-0.55,0.75,0.3846,0.3846);

	this.instance_7 = new lib.CachedTexturedBitmap_90();
	this.instance_7.parent = this;
	this.instance_7.setTransform(-0.05,1.3,0.3846,0.3846);

	this.instance_8 = new lib.CachedTexturedBitmap_91();
	this.instance_8.parent = this;
	this.instance_8.setTransform(-0.2,1.3,0.3846,0.3846);

	this.instance_9 = new lib.CachedTexturedBitmap_92();
	this.instance_9.parent = this;
	this.instance_9.setTransform(0.05,1.3,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,-0.1,97.1,96.69999999999999);


(lib.mouth_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_76();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_77();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0.05,0.45,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_78();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.05,0,0.3846,0.3846);

	this.instance_3 = new lib.CachedTexturedBitmap_79();
	this.instance_3.parent = this;
	this.instance_3.setTransform(0.65,0.55,0.3846,0.3846);

	this.instance_4 = new lib.CachedTexturedBitmap_80();
	this.instance_4.parent = this;
	this.instance_4.setTransform(0.2,0.8,0.3846,0.3846);

	this.instance_5 = new lib.CachedTexturedBitmap_81();
	this.instance_5.parent = this;
	this.instance_5.setTransform(1.05,1.15,0.3846,0.3846);

	this.instance_6 = new lib.CachedTexturedBitmap_82();
	this.instance_6.parent = this;
	this.instance_6.setTransform(1.8,1.65,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,86.6,77.7);


(lib.mouth_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_69();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_70();
	this.instance_1.parent = this;
	this.instance_1.setTransform(2.3,0.3,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_71();
	this.instance_2.parent = this;
	this.instance_2.setTransform(5.5,0,0.3846,0.3846);

	this.instance_3 = new lib.CachedTexturedBitmap_72();
	this.instance_3.parent = this;
	this.instance_3.setTransform(6.8,0.2,0.3846,0.3846);

	this.instance_4 = new lib.CachedTexturedBitmap_73();
	this.instance_4.parent = this;
	this.instance_4.setTransform(7.3,0.05,0.3846,0.3846);

	this.instance_5 = new lib.CachedTexturedBitmap_74();
	this.instance_5.parent = this;
	this.instance_5.setTransform(5.2,0.05,0.3846,0.3846);

	this.instance_6 = new lib.CachedTexturedBitmap_75();
	this.instance_6.parent = this;
	this.instance_6.setTransform(4.2,0,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,121.2,88.1);


(lib.mouth_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_66();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_67();
	this.instance_1.parent = this;
	this.instance_1.setTransform(1.2,0.2,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_68();
	this.instance_2.parent = this;
	this.instance_2.setTransform(1.05,1.75,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,86.9,89.5);


(lib.lines_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_61();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_62();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-7.7,-26.7,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_63();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-22.4,-44.95,0.3846,0.3846);

	this.instance_3 = new lib.CachedTexturedBitmap_64();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-32.8,-58.9,0.3846,0.3846);

	this.instance_4 = new lib.CachedTexturedBitmap_65();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-40.15,-64.9,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-40.1,-64.9,291.2,181.10000000000002);


(lib.lines_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_57();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_58();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-21.55,-0.95,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_59();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-48.45,-2.45,0.3846,0.3846);

	this.instance_3 = new lib.CachedTexturedBitmap_60();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-66.55,-3.75,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-66.5,-3.7,151.5,157.29999999999998);


(lib.lines_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_53();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_54();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-18.55,-36.15,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_55();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-34.4,-58.2,0.3846,0.3846);

	this.instance_3 = new lib.CachedTexturedBitmap_56();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-45.3,-73.7,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-45.3,-73.7,291.6,191);


(lib.leg_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_50();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_51();
	this.instance_1.parent = this;
	this.instance_1.setTransform(1.8,6.15,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_52();
	this.instance_2.parent = this;
	this.instance_2.setTransform(1.45,3.95,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,85,101.3);


(lib.leg_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_47();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_48();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0.55,-4.3,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_49();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0.45,7.05,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-4.3,46.9,108.7);


(lib.leg_01 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_44();
	this.instance.parent = this;
	this.instance.setTransform(0,-1.7,0.3117,0.3117);

	this.instance_1 = new lib.CachedTexturedBitmap_45();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0.7,2.7,0.3117,0.3117);

	this.instance_2 = new lib.CachedTexturedBitmap_46();
	this.instance_2.parent = this;
	this.instance_2.setTransform(1.25,-1.6,0.3117,0.3117);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-1.7,38.4,90.10000000000001);


(lib.head_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_41();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_42();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0.7,0.7,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_43();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0.7,0.3,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,143.1,116.9);


(lib.head_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_38();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_39();
	this.instance_1.parent = this;
	this.instance_1.setTransform(1.35,0.7,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_40();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0.7,0.95,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,154.8,122.9);


(lib.head_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_35();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_36();
	this.instance_1.parent = this;
	this.instance_1.setTransform(2.05,0.15,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_37();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.05,1.3,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,145.2,137.5);


(lib.head_01 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_32();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_33();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0.55,-0.05,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_34();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0.2,-1.05,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-1,140,130);


(lib.body_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_29();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_30();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0.2,-3.65,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_31();
	this.instance_2.parent = this;
	this.instance_2.setTransform(3.7,2.45,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-3.6,93.5,119);


(lib.body_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_26();
	this.instance.parent = this;
	this.instance.setTransform(0,-0.35,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_27();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0.55,-0.15,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_28();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0.2,-0.75,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-0.7,44.8,124.8);


(lib.body_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_23();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_24();
	this.instance_1.parent = this;
	this.instance_1.setTransform(5.3,-6.3,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_25();
	this.instance_2.parent = this;
	this.instance_2.setTransform(6.05,-5.3,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-6.3,93.1,118.5);


(lib.body_01 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_20();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_21();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.3,-2.2,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_22();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0.8,-0.05,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.3,-2.2,69.2,117.2);


(lib.arm_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_17();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_18();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.35,-5.9,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_19();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.45,-2.3,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.4,-5.9,166.6,52.1);


(lib.arm_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_14();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_15();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-1.45,-9.95,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_16();
	this.instance_2.parent = this;
	this.instance_2.setTransform(6.2,3.65,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.4,-9.9,90.7,131.9);


(lib.arm_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_11();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_12();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-5.3,-1.55,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_13();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.55,-0.6,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.3,-1.5,106.3,73.2);


(lib.arm_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_8();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0.55,-4.65,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_10();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.3,-4.8,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.3,-4.8,87.8,84.3);


(lib.arm_02 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_5();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.363,0.363);

	this.instance_1 = new lib.CachedTexturedBitmap_6();
	this.instance_1.parent = this;
	this.instance_1.setTransform(1.55,-0.5,0.363,0.363);

	this.instance_2 = new lib.CachedTexturedBitmap_7();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.55,-1.6,0.363,0.363);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-1.6,174.6,75.69999999999999);


(lib.arm_01 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CachedTexturedBitmap_2();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.3846,0.3846);

	this.instance_1 = new lib.CachedTexturedBitmap_3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-5.05,-0.3,0.3846,0.3846);

	this.instance_2 = new lib.CachedTexturedBitmap_4();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-17.15,-0.6,0.3846,0.3846);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-17.1,-0.6,143.4,157.7);


(lib.Scene_1_Layer_9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_9
	this.instance = new lib.leg_01("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(838.05,877.15,1.3,1.3805,0,6.3305,0,19.2,88.4);

	this.instance_1 = new lib.leg_5("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(843.85,877.1,1.3,1.3,0,0,180,22.4,103.1);

	this.instance_2 = new lib.leg_6("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(822.05,866.45,1.3,1.2159,0,-13.0143,0,20.6,102.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},20).to({state:[{t:this.instance}]},13).to({state:[{t:this.instance}]},7).to({state:[{t:this.instance}]},4).to({state:[{t:this.instance}]},7).to({state:[{t:this.instance}]},4).to({state:[{t:this.instance}]},11).to({state:[{t:this.instance_1}]},6).to({state:[{t:this.instance_2}]},41).to({state:[{t:this.instance}]},40).wait(42));
	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleY:1.3721,skewX:0,startPosition:2},20,cjs.Ease.get(1)).wait(13).to({scaleY:1.5707,skewX:17.64,skewY:180,x:828.1,y:884.25},0).to({scaleY:1.5588,skewX:16.2047,x:828.15,startPosition:3},7,cjs.Ease.get(0.97)).to({regX:19.1,scaleY:1.5958,skewX:17.9199,x:828.2,y:884.3,startPosition:1},4,cjs.Ease.get(1)).to({regX:19.2,scaleY:1.5588,skewX:16.2047,x:828.15,y:884.25,startPosition:2},7,cjs.Ease.get(1)).to({regX:19.1,scaleY:1.6043,skewX:17.9876,x:828.2,y:884.3,startPosition:0},4,cjs.Ease.get(1)).to({regX:19.2,scaleY:1.5588,skewX:16.2047,x:828.15,y:884.25,startPosition:5},11,cjs.Ease.get(1)).to({_off:true},6).wait(81).to({_off:false,scaleX:1.303,scaleY:1.4636,skewX:-14.6326,skewY:3.9086,x:845.9,y:875.55,startPosition:3},0).wait(42));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.instance = new lib.arm_01("synched",3);
	this.instance.parent = this;
	this.instance.setTransform(843.6,666.8,1.3,1.3,34.1316,0,0,0,149.6);

	this.instance_1 = new lib.arm_4("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(815.2,622.7,1.3,1.3,-14.7411,0,0,9.3,50.6);
	this.instance_1._off = true;

	this.instance_2 = new lib.arm_5("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(814.45,622.55,1.3,1.3,0,0,0,14.5,8.6);
	this.instance_2._off = true;

	this.instance_3 = new lib.arm_6("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(764.6,647.2,1.3,1.3,-12.4527,0,0,143.7,10);
	this.instance_3._off = true;

	this.instance_4 = new lib.arm_02("synched",2);
	this.instance_4.parent = this;
	this.instance_4.setTransform(808.85,634.3,1.2144,1.2999,0,46.1405,-133.8576,165.7,46.1);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:-4.5227,x:816.75,y:665.5,startPosition:5},20,cjs.Ease.get(1)).to({_off:true},13).wait(162));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(33).to({_off:false},0).to({regX:9.4,rotation:0,x:793.5,y:626.5,startPosition:1},7,cjs.Ease.get(0.97)).to({regX:9.3,regY:50.5,rotation:-12.7306,x:817.45,y:622.5,startPosition:2},4,cjs.Ease.get(1)).to({rotation:12.95,x:793.5,y:626.45,startPosition:0},7,cjs.Ease.get(1)).to({regX:9.4,regY:50.6,rotation:-13.7638,x:811.15,y:624.6,startPosition:1},4,cjs.Ease.get(1)).to({regX:9.3,regY:50.5,rotation:12.1987,x:793.5,y:626.4,startPosition:0},11,cjs.Ease.get(1)).to({_off:true},6).wait(123));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(72).to({_off:false},0).to({regY:8.7,rotation:-57.4424,x:814.5,y:622.5,startPosition:3},33,cjs.Ease.get(1)).to({_off:true},8).wait(82));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(113).to({_off:false},0).to({rotation:2.2495,y:647.25,startPosition:3},3).to({regY:9.9,scaleX:1.2999,scaleY:1.2999,rotation:-29.9832,y:647.15,startPosition:0},3).to({regY:10,scaleX:1.3,scaleY:1.3,rotation:-13.287,x:764.65,y:647.2,startPosition:3},3).to({rotation:-21.0196,x:764.6,y:647.25,startPosition:0},3).to({scaleX:1.2999,scaleY:1.2999,rotation:-9.7893,y:647.2,startPosition:3},3).to({regY:10.1,scaleX:1.3,scaleY:1.3,rotation:-39.0124,x:764.65,startPosition:1},4).to({regY:10,rotation:-15.5178,y:647.15,startPosition:4},3).to({regX:143.6,regY:10.1,rotation:-26.9621,x:764.55,y:647.2,startPosition:1},3).to({regY:10.2,scaleX:1.2999,scaleY:1.2999,rotation:-9.7444,x:764.6,y:647.25,startPosition:5},4).to({regY:10.1,rotation:-30.2261,y:647.2,startPosition:2},3).to({scaleX:1.3,scaleY:1.3,rotation:-10.0378,x:764.55,startPosition:5},3).to({regY:10.2,scaleX:1.2999,scaleY:1.2999,rotation:-24.9837,x:764.6,y:647.25,startPosition:2},4).to({_off:true,regX:165.7,regY:46.1,scaleX:1.2144,rotation:0,skewX:46.1405,skewY:-133.8576,x:808.85,y:634.3},1).wait(42));
	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(152).to({_off:false},1).wait(10).to({startPosition:0},0).wait(1).to({regX:87.3,regY:36.2,skewX:46.4996,skewY:-133.4985,x:883.65,y:694.5,startPosition:1},0).wait(1).to({skewX:47.7465,skewY:-132.2516,x:882.35,y:696.1,startPosition:2},0).wait(1).to({skewX:50.137,skewY:-129.8611,x:879.75,y:699.15,startPosition:3},0).wait(1).to({skewX:53.6083,skewY:-126.3899,x:875.65,y:703.35,startPosition:4},0).wait(1).to({scaleY:1.3,skewX:57.2216,skewY:-122.7765,x:871.2,y:707.45,startPosition:5},0).wait(1).to({scaleY:1.2999,skewX:59.8447,skewY:-120.1535,x:867.75,y:710.3,startPosition:0},0).wait(1).to({skewX:61.2323,skewY:-118.7659,x:865.9,y:711.65,startPosition:1},0).wait(1).to({regX:165.7,regY:46.1,scaleY:1.3,skewX:61.6312,skewY:-118.367,x:808.9,y:634.3,startPosition:2},0).wait(24));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.lines_1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(864.3,438.35,1.3,1.3,18.1989,0,0,101.4,58.6);

	this.instance_1 = new lib.lines_2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(575.8,623.55,1.3,1.3,16.4533,0,0,42.5,53);
	this.instance_1._off = true;

	this.instance_2 = new lib.lines_3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(782.45,443.55,1.3,1.3,0,0,0,112.4,58.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},20).to({state:[{t:this.instance_1}]},13).to({state:[{t:this.instance_1}]},7).to({state:[{t:this.instance_1}]},4).to({state:[{t:this.instance_1}]},7).to({state:[{t:this.instance_1}]},4).to({state:[{t:this.instance_1}]},11).to({state:[{t:this.instance_2}]},6).to({state:[{t:this.instance_1}]},41).to({state:[{t:this.instance_1}]},40).wait(42));
	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:0,x:763.9,y:432.5},20,cjs.Ease.get(1)).to({_off:true},13).wait(162));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(33).to({_off:false},0).to({regX:42.6,regY:52.9,rotation:0,x:549.7,y:702.9,startPosition:7},7,cjs.Ease.get(0.97)).to({regX:42.5,rotation:19.6609,x:565.45,y:614.9,startPosition:1},4,cjs.Ease.get(1)).to({regX:42.6,rotation:0,x:549.7,y:702.9,startPosition:8},7,cjs.Ease.get(1)).to({rotation:17.9154,x:559.15,y:625.35,startPosition:2},4,cjs.Ease.get(1)).to({rotation:0,x:545.8,y:719.8,startPosition:3},11,cjs.Ease.get(1)).to({_off:true},6).wait(41).to({_off:false,regX:42.5,skewX:-64.6603,skewY:115.3397,x:899.4,y:487.5,startPosition:1},0).wait(40).to({skewX:-19.6609,skewY:160.3391,x:1051.6,y:628.45},0).wait(42));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.arm_02("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(808.7,667.5,1.3,1.3,-0.7559,0,0,165.5,46.1);

	this.instance_1 = new lib.arm_3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(792.6,646.35,1.3,1.3,32.4022,0,0,86.8,0.1);
	this.instance_1._off = true;

	this.instance_2 = new lib.arm_5("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(775.45,622.55,1.3,1.3,0,0,180,14.4,8.6);
	this.instance_2._off = true;

	this.instance_3 = new lib.arm_6("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(780.05,639.5,1.3,1.3,0,-26.467,153.533,143.7,10);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:165.6,scaleX:1.3773,rotation:14.2465,x:782.35,y:671.15,startPosition:2},20,cjs.Ease.get(0.98)).to({_off:true},13).wait(119).to({_off:false,scaleX:1.24,rotation:0,skewX:22.6898,skewY:-157.3089,x:826.65,y:644.9},1).wait(10).to({startPosition:0},0).wait(1).to({regX:87.3,regY:36.2,scaleX:1.2378,skewX:23.2788,skewY:-156.6903,x:920.7,y:671.4,startPosition:1},0).wait(1).to({scaleX:1.23,skewX:25.324,skewY:-154.5426,x:919.05,y:674.65,startPosition:2},0).wait(1).to({scaleX:1.215,scaleY:1.3001,skewX:29.245,skewY:-150.4251,x:915.6,y:680.55,startPosition:3},0).wait(1).to({scaleX:1.1933,skewX:34.9386,skewY:-144.446,x:909.95,y:688.7,startPosition:4},0).wait(1).to({scaleX:1.1706,scaleY:1.3002,skewX:40.8654,skewY:-138.2221,x:903.35,y:696.2,startPosition:5},0).wait(1).to({scaleX:1.1542,skewX:45.1678,skewY:-133.704,x:898.15,y:701.15,startPosition:0},0).wait(1).to({scaleX:1.1455,skewX:47.4438,skewY:-131.3138,x:895.3,y:703.6,startPosition:1},0).wait(1).to({regX:165.6,regY:46.1,scaleX:1.143,skewX:48.0981,skewY:-130.6268,x:826.6,y:644.85,startPosition:2},0).wait(24));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(33).to({_off:false},0).to({rotation:0,x:774.55,y:653.35,startPosition:1},7,cjs.Ease.get(0.97)).to({rotation:27.7095,x:794.5,y:645.95,startPosition:2},4,cjs.Ease.get(1)).to({regY:0,rotation:-8.4995,x:774.55,y:653.2,startPosition:0},7,cjs.Ease.get(1)).to({rotation:23.1632,x:789.3,y:648.95,startPosition:1},4,cjs.Ease.get(1)).to({regY:0.1,rotation:-5.7516,x:774.55,y:653.35,startPosition:0},11,cjs.Ease.get(1)).to({_off:true},6).wait(123));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(72).to({_off:false},0).to({regX:14.3,regY:8.7,skewX:47.9383,skewY:227.9383,x:775.4,y:622.7,startPosition:3},33,cjs.Ease.get(1)).to({_off:true},8).wait(82));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(113).to({_off:false},0).to({skewX:-39.7115,skewY:140.2885,x:780.1,startPosition:3},3).to({regX:143.6,scaleX:1.2999,scaleY:1.2999,skewX:-19.9745,skewY:160.0255,x:780.15,y:639.45,startPosition:0},3).to({skewX:-34.4453,skewY:145.5547,x:780.1,startPosition:3},3).to({scaleX:1.3,scaleY:1.3,skewX:-24.7144,skewY:155.2856,x:780.15,startPosition:0},3).to({scaleX:1.2999,scaleY:1.2999,skewX:-37.962,skewY:142.038,x:780.2,startPosition:3},3).to({scaleX:1.3,scaleY:1.3,skewX:-16.7344,skewY:163.2656,x:780.15,startPosition:1},4).to({scaleX:1.2999,scaleY:1.2999,skewX:-31.7037,skewY:148.2963,startPosition:4},3).to({skewX:-22.2513,skewY:157.7487,x:780.05,y:639.5,startPosition:1},3).to({skewX:-35.4575,skewY:144.5425,x:780.1,startPosition:5},4).to({regX:143.5,regY:9.9,scaleX:1.3,scaleY:1.3,skewX:-20.0109,skewY:159.9891,x:780.05,y:639.35,startPosition:2},3).to({regY:10,scaleX:1.2999,scaleY:1.2999,skewX:-33.4765,skewY:146.5235,y:639.45,startPosition:5},3).to({scaleX:1.3,scaleY:1.3,skewX:-23.2623,skewY:156.7377,y:639.35,startPosition:2},4).to({_off:true,regX:165.6,regY:46.1,scaleX:1.24,skewX:22.6898,skewY:202.6911,x:826.65,y:644.9},1).wait(42));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.leg_01("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(777.6,884.4,1.3,1.3587,0,6.3096,0,19.2,88.5);

	this.instance_1 = new lib.leg_5("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(758.65,877.1,1.3,1.3,0,0,0,22.4,103.1);

	this.instance_2 = new lib.leg_6("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(756.75,879.5,1.3,1.3,0,0,0,20.4,102.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},20).to({state:[{t:this.instance}]},13).to({state:[{t:this.instance}]},7).to({state:[{t:this.instance}]},4).to({state:[{t:this.instance}]},7).to({state:[{t:this.instance}]},4).to({state:[{t:this.instance}]},11).to({state:[{t:this.instance_1}]},6).to({state:[{t:this.instance_2}]},41).to({state:[{t:this.instance}]},40).wait(42));
	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:88.4,scaleY:1.3,skewX:0,y:884.25,startPosition:2},20,cjs.Ease.get(1)).wait(13).to({regX:19.1,scaleY:1.4967,skewX:21.6692,skewY:180,x:765.2,y:872.55},0).to({startPosition:3},7,cjs.Ease.get(0.97)).to({regY:88.5,scaleY:1.6013,skewX:21.6696,x:765.15,y:872.65,startPosition:1},4,cjs.Ease.get(1)).to({regY:88.4,scaleY:1.4967,skewX:21.6692,x:765.2,y:872.55,startPosition:2},7,cjs.Ease.get(1)).to({scaleY:1.5614,skewX:21.8919,y:872.5,startPosition:0},4,cjs.Ease.get(1)).to({scaleY:1.4967,skewX:21.6692,y:872.55,startPosition:5},11,cjs.Ease.get(1)).to({_off:true},6).wait(81).to({_off:false,regX:19.2,scaleX:1.303,scaleY:1.4227,skewX:-8.8427,skewY:3.9086,x:777.65,y:883.8,startPosition:3},0).wait(42));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.instance = new lib.body_01("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(826,783.75,1.3,1.3,7.9885,0,0,34.4,114.2);

	this.instance_1 = new lib.body_2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(830.4,771.8,1.3,1.3,8.458,0,0,65.6,109.2);
	this.instance_1._off = true;

	this.instance_2 = new lib.body_3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(800.35,775.75,1.3,1.3,0,0,0,21.6,124.3);

	this.instance_3 = new lib.body_5("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(853,777.75,1.3,1.3,0,0,0,72.3,115);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},20).to({state:[{t:this.instance_1}]},13).to({state:[{t:this.instance_1}]},7).to({state:[{t:this.instance_1}]},4).to({state:[{t:this.instance_1}]},7).to({state:[{t:this.instance_1}]},4).to({state:[{t:this.instance_1}]},11).to({state:[{t:this.instance_2}]},6).to({state:[{t:this.instance_3}]},41).to({state:[{t:this.instance_1}]},40).wait(42));
	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:0,x:815.6,y:783.7,startPosition:2},20,cjs.Ease.get(1)).to({_off:true},13).wait(162));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(33).to({_off:false},0).to({regY:109.3,rotation:0,y:771.9,startPosition:1},7,cjs.Ease.get(0.97)).to({regY:109.2,rotation:9.2212,x:830.65,y:771.85,startPosition:5},4,cjs.Ease.get(1)).to({regY:109.3,rotation:0,x:830.4,y:771.9,startPosition:0},7,cjs.Ease.get(1)).to({rotation:6.717,x:830.8,y:773.2,startPosition:4},4,cjs.Ease.get(1)).to({rotation:0,x:830.4,y:771.9,startPosition:3},11,cjs.Ease.get(1)).to({_off:true},6).wait(81).to({_off:false,regX:65.5,regY:109.2,skewX:-8.458,skewY:171.542,x:796.7,y:778.25,startPosition:0},0).wait(42));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.mouth1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(838.3,591.4,1.3,1.3,18.1989,0,0,48.3,48.3);

	this.instance_1 = new lib.mouth_2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(739.35,562.45,1.3,1.3,23.7003,0,0,43.4,43.8);
	this.instance_1._off = true;

	this.instance_2 = new lib.mouth_3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(786,547.75,1.3,1.3,0,0,0,60.6,44.1);

	this.instance_3 = new lib.mouth_4("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(778.5,580.5,1.3,1.3,0,0,0,43.3,38.9);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},20).to({state:[{t:this.instance_1}]},13).to({state:[{t:this.instance_1}]},7).to({state:[{t:this.instance_1}]},4).to({state:[{t:this.instance_1}]},7).to({state:[{t:this.instance_1}]},4).to({state:[{t:this.instance_1}]},11).to({state:[{t:this.instance_2}]},6).to({state:[{t:this.instance_3}]},41).to({state:[{t:this.instance_3}]},40).to({state:[{t:this.instance_3}]},10).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_3}]},1).wait(24));
	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:0,x:786.9,y:586.1},20,cjs.Ease.get(1)).to({_off:true},13).wait(162));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(33).to({_off:false},0).to({regY:43.9,rotation:0,x:693.05,y:600.85,startPosition:1},7,cjs.Ease.get(0.97)).to({regX:43.3,rotation:19.6609,x:734.8,y:567.05,startPosition:2},4,cjs.Ease.get(1)).to({regX:43.4,rotation:0,x:693.05,y:600.85,startPosition:0},7,cjs.Ease.get(1)).to({regY:43.8,rotation:17.9154,x:727.15,y:572.15,startPosition:1},4,cjs.Ease.get(1)).to({regY:43.9,rotation:0,x:689.15,y:617.75,startPosition:0},11,cjs.Ease.get(1)).to({_off:true},6).wait(123));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(113).to({_off:false},0).wait(40).to({regX:43.2,scaleX:1.157,rotation:35.1892,x:892,y:606.7},0).wait(10).to({startPosition:3},0).wait(1).to({regY:38.8,rotation:35.6225,x:892.3,y:607.05,startPosition:4},0).wait(1).to({rotation:37.1272,x:893.15,y:608.8,startPosition:5},0).wait(1).to({rotation:40.0118,x:894.8,y:612.25,startPosition:6},0).wait(1).to({rotation:44.2007,x:897.2,y:617.1,startPosition:0},0).wait(1).to({rotation:48.561,x:899.7,y:622.2,startPosition:1},0).wait(1).to({rotation:51.7263,x:901.45,y:625.95,startPosition:2},0).wait(1).to({rotation:53.4008,x:902.4,y:627.9,startPosition:3},0).wait(1).to({regX:43.1,regY:38.9,rotation:53.8821,x:902.5,y:628.55,startPosition:4},0).wait(24));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.head_01("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(833.65,665.45,1.3,1.3,18.1989,0,0,88,118.8);

	this.instance_1 = new lib.head_2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(792.95,643.05,1.3,1.3,23.7003,0,0,118.4,115.5);
	this.instance_1._off = true;

	this.instance_2 = new lib.head_3("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(794.1,631.15,1.3,1.3,0,0,0,82.4,121.2);

	this.instance_3 = new lib.head_5("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(789.9,635.3,1.3,1.3,0,0,0,96.6,106.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},20).to({state:[{t:this.instance_1}]},13).to({state:[{t:this.instance_1}]},7).to({state:[{t:this.instance_1}]},4).to({state:[{t:this.instance_1}]},7).to({state:[{t:this.instance_1}]},4).to({state:[{t:this.instance_1}]},11).to({state:[{t:this.instance_2}]},6).to({state:[{t:this.instance_3}]},41).to({state:[{t:this.instance_3}]},40).to({state:[{t:this.instance_3}]},10).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_3}]},1).wait(24));
	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:88.1,regY:118.9,rotation:0,x:805.8,y:657.85,startPosition:2},20,cjs.Ease.get(1)).to({_off:true},13).wait(162));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(33).to({_off:false},0).to({rotation:0,x:774.5,y:653.2,startPosition:1},7,cjs.Ease.get(0.97)).to({rotation:19.6609,x:794.05,y:643.8,startPosition:5},4,cjs.Ease.get(1)).to({rotation:0,x:774.5,y:653.2,startPosition:0},7,cjs.Ease.get(1)).to({rotation:17.9154,x:788.4,y:647.2,startPosition:4},4,cjs.Ease.get(1)).to({rotation:0,x:770.6,y:670.1,startPosition:3},11,cjs.Ease.get(1)).to({_off:true},6).wait(123));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(113).to({_off:false},0).wait(40).to({regX:64,regY:114.8,rotation:25.9283,x:829,y:648.55},0).wait(10).to({startPosition:4},0).wait(1).to({regX:71.5,regY:58.5,rotation:26.3617,x:870.3,y:587.25,startPosition:5},0).wait(1).to({rotation:27.8662,x:871.85,y:588.35,startPosition:0},0).wait(1).to({rotation:30.7507,x:874.9,y:590.45,startPosition:1},0).wait(1).to({rotation:34.9392,x:879.1,y:593.95,startPosition:2},0).wait(1).to({rotation:39.2992,x:883.25,y:597.75,startPosition:3},0).wait(1).to({rotation:42.4643,x:885.95,y:600.75,startPosition:4},0).wait(1).to({rotation:44.1387,x:887.4,y:602.4,startPosition:5},0).wait(1).to({regX:64,regY:114.8,rotation:44.62,x:829.5,y:648.15,startPosition:0},0).wait(24));

}).prototype = p = new cjs.MovieClip();


// stage content:
(lib.complain = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// timeline functions:
	this.frame_194 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(194).call(this.frame_194).wait(1));

	// Layer_7_obj_
	this.Layer_7 = new lib.Scene_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.setTransform(864.3,438.4,1,1,0,0,0,864.3,438.4);
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 0
	this.Layer_7.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(195));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(838.3,591.4,1,1,0,0,0,838.3,591.4);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 1
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(164).to({regX:802.9,regY:593.2,x:802.9,y:593.2},0).wait(7).to({regX:838.3,regY:591.4,x:838.3,y:591.4},0).wait(24));

	// Layer_1_obj_
	this.Layer_1 = new lib.Scene_1_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(833.6,591,1,1,0,0,0,833.6,591);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 2
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(164).to({regX:812.1,regY:583,x:812.1,y:583},0).wait(7).to({regX:833.6,regY:591,x:833.6,y:591},0).wait(24));

	// Layer_8_obj_
	this.Layer_8 = new lib.Scene_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.setTransform(962.1,631.6,1,1,0,0,0,962.1,631.6);
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 3
	this.Layer_8.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(164).to({regX:832.3,regY:643.3,x:832.3,y:643.3},0).wait(7).to({regX:962.1,regY:631.6,x:962.1,y:631.6},0).wait(24));

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.setTransform(704,657.1,1,1,0,0,0,704,657.1);
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 4
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(164).to({regX:796.2,regY:658.3,x:796.2,y:658.3},0).wait(7).to({regX:704,regY:657.1,x:704,y:657.1},0).wait(24));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(836.4,710.3,1,1,0,0,0,836.4,710.3);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 5
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(195));

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.setTransform(784.3,823.5,1,1,0,0,0,784.3,823.5);
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 6
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(195));

	// Layer_9_obj_
	this.Layer_9 = new lib.Scene_1_Layer_9();
	this.Layer_9.name = "Layer_9";
	this.Layer_9.parent = this;
	this.Layer_9.setTransform(844.9,815.3,1,1,0,0,0,844.9,815.3);
	this.Layer_9.depth = 0;
	this.Layer_9.isAttachedToCamera = 0
	this.Layer_9.isAttachedToMask = 0
	this.Layer_9.layerDepth = 0
	this.Layer_9.layerIndex = 7
	this.Layer_9.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_9).wait(195));

	// Layer_6_obj_
	this.Layer_6 = new lib.Scene_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.setTransform(966.3,540.3,1,1,0,0,0,966.3,540.3);
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 8
	this.Layer_6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(195));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(956.1,528.1,980.4999999999999,564.4999999999999);
// library properties:
lib.properties = {
	id: 'DBCF370E9B414995BBCAC3970CE125D5',
	width: 1920,
	height: 1080,
	fps: 30,
	color: "#333333",
	opacity: 1.00,
	manifest: [
		{src:"images/CachedTexturedBitmap_1.png", id:"CachedTexturedBitmap_1"},
		{src:"images/complain_atlas_.png", id:"complain_atlas_"},
		{src:"images/complain_atlas_2.png", id:"complain_atlas_2"},
		{src:"images/complain_atlas_3.png", id:"complain_atlas_3"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['DBCF370E9B414995BBCAC3970CE125D5'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;