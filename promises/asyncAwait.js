// https://alligator.io/js/async-functions/

// 

var callCount = 0;

function promise01(callCount, timeout) {
	return new Promise(resolve => {
	var timeout = timeout || Math.floor(Math.random() * 300);
		setTimeout(() => {
			let result = `${callCount}) result from promise01!`;
			resolve(result);
		}, timeout);
	});
}


// Can return in any order
console.log("Executing as promises");
promise01(++callCount).then(result => {console.log(result)});
promise01(++callCount).then(result => {console.log(result)});
promise01(++callCount).then(result => {console.log(result)});
promise01(++callCount).then(result => {console.log(result)});
promise01(++callCount).then(result => {console.log(result)});
promise01(++callCount).then(result => {console.log(result)});

// Returns in order but it takes as long as each of the execution times added together
console.log("Executing using async await");
async function asAwaitSequence() {
	let a = await promise01(++callCount);
	let b = await promise01(++callCount);
	let c = await promise01(++callCount);
	let d = await promise01(++callCount);
	let e = await promise01(++callCount);
	let f = await promise01(++callCount, 1000);

	console.log("_____ asAwaitSequence() result:");
	console.log(a);
	console.log(b);
	console.log(c);
	console.log(d);
	console.log(e);
	console.log(f);
	console.log("________________________________");	
}
asAwaitSequence();

// Returns in order and as it's executed in parallel, it'll finish sooner than the sequence version
console.log("executing in parallel using async")
async function asAwaitParallel() {
	const [a, b, c, d, e, f] = await Promise.all([promise01(++callCount), promise01(++callCount), promise01(++callCount), promise01(++callCount), promise01(++callCount), promise01(++callCount, 1000)]);

	console.log("_____ asAwaitParallel() result:");
	console.log(`${ a }\n${ b }\n${ c }\n${ d }\n${ e }\n${ f }`);
	console.log("________________________________");
}
asAwaitParallel();