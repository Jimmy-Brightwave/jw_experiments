import { request } from "http";
const got = require('got');

class Main
{
	private _zipUrl = "http://www.mazemod.org/index.php/playlist/50.zip";
	
	constructor()
	{
		console.log("Hello world");

		// request(this._testLink, {}, res => {
		// 	console.log(res);
		// });
		this.getLink();
	}

	private async getLink()
	{
		var testResponse = await got(this._zipUrl, { json: false });
		console.log(`Test response!: ${ testResponse }`);
	
	}

}

var main = new Main();

// Making http requests can be done in several ways

